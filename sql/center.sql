-- auto-generated definition
create table gardens_user_terminal
(
    user_terminal_id int auto_increment
        primary key,
    user_id          int not null,
    terminal_id      int not null
);

-- auto-generated definition
create table gardens_terminal
(
    terminal_id         int auto_increment
        primary key,
    terminal_mac        varchar(255) not null,
    terminal_last_login datetime     null
);

