package com.ruoyi.project.secret.domain;

import lombok.Data;

import java.util.List;

/**
 * Secret信息模板
 *
 * @author Squbi
 */
@Data
public class SecretBase {
    private long total;
    private long used;
    private int level;
    private String mac_address;
    private List<String> imported;
}
