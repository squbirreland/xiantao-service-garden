package com.ruoyi.project.secret.service;

import com.ruoyi.project.secret.domain.SecretBase;
import com.ruoyi.project.secret.domain.SecretResult;

/**
 * @author Squbi
 */
public interface SecretServer {
    /**
     * 刷新Secret的信息
     *
     * @author Squbi
     * @date 2021/7/7 11:49
     */
    void flushSecretInfo();

    /**
     * 线程后台执行轮询刷新
     *
     * @author Squbi
     * @date 2021/8/2 15:26
     */
    void runListen();

    /**
     * 获取Secret信息
     *
     * @return {@link SecretBase}
     * @author Squbi
     * @date 2021/8/2 15:18
     */
    SecretBase getSecretBase();

    String leftTime();

    /**
     * 给予指定加密码
     *
     * @param code 加密码
     * @return {@link SecretResult}
     * @author Squbi
     * @date 2021/7/7 11:55
     */
    SecretResult pushCode(String code);
}
