package com.ruoyi.project.common;

/**
 * 返回信息枚举
 *
 * @author Squbi
 * @date 2021/3/30 16:52
 */
public enum ResultEnum {
    //- 1常规
    SUCCESS(1001, "成功"),
    //- 2系统
    SYS_ERROR(2001, "系统错误"),
    WX_ERROR(2002, "微信调用错误"),
    TIME_PARSE_ERROR(2003, "时间格式错误"),
    DB_CONN_ERROR(2004, "数据库连接错误"),
    DB_CLOSE_ERROR(2005, "数据库关闭错误"),
    DB_INNER_ERROR(2006, "数据库内部错误"),
    CALL_ERROR(2007, "调用发送异常"),
    //- 3权限
    NOT_LOGIN(3001, "当前未登录"),
    TOKEN_TIMEOUT(3001, "登录已过期"),
    TOKEN_INVALID(3001, "无效令牌"),
    ALREADY_LOGIN(3002, "用户已登录"),
    //- 4数据
    ARGS_ERR(4001, "参数错误"),
    REQUEST_NULL(4002, "请求的数据不存在"),
    MQTT_BAD_CALLBACK(4003,"mqtt回调错误");
    //- 5业务

    private final int code;
    private final String msg;

    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
