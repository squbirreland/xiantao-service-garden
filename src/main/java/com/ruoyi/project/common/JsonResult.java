package com.ruoyi.project.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Json返回对象
 *
 * @author Squbi
 * @date 2021/3/29 12:36
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JsonResult implements Serializable {
    private String uuid;
    private boolean success;
    private int code;
    private String message;
    private Object data;

    // --- 构造方法 ---
    public static JsonResult success(String message, Object data) {
        return JsonResult.builder()
                .success(true)
                .code(ResultEnum.SUCCESS.getCode())
                .message(message)
                .data(data)
                .build();
    }

    public static JsonResult success(Object data) {
        return success(ResultEnum.SUCCESS.getMsg(), data);
    }

    public static JsonResult success() {
        return success(null);
    }

    public static JsonResult failed(int code, String message) {
        return JsonResult.builder()
                .success(false)
                .code(code)
                .message(message)
                .data(null)
                .build();
    }

    public static JsonResult failed(ResultEnum resultEnum) {
        return JsonResult.builder()
                .success(false)
                .code(resultEnum.getCode())
                .message(resultEnum.getMsg())
                .data(null)
                .build();
    }

    public static JsonResult failed(ResultEnum resultEnum, String msg) {
        return JsonResult.builder()
                .success(false)
                .code(resultEnum.getCode())
                .message(resultEnum.getMsg())
                .data(msg)
                .build();
    }
}
