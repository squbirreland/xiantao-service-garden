package com.ruoyi.project.test.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.test.domain.TestInfo;
import com.ruoyi.project.test.domain.TestInfoVo;
import com.ruoyi.project.test.service.TestService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Squbi
 */
@RestController
@RequestMapping("/open-api/test")
//@Api(tags = "测试")
public class Test1Controller extends BaseController {
    @Autowired
    private TestService testService;


    @RequestMapping("query")
//    @ApiOperation("查询")
    public TableDataInfo testQuery(){
//        Page page = new Page();
//        page.setCurrent(2);
//        page.setSize(5);
        startPage();
        QueryWrapper<TestInfo> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.like("name","zs");
        List<TestInfo> list = testService.list(objectQueryWrapper);
//        TableDataInfo dataTable = getDataTable(list,TestInfoVo.class);
        TableDataInfo dataTable = getDataTable(list,(item)->{
            TestInfoVo testInfoVo = new TestInfoVo();
            testInfoVo.setName(item.getName());
            return testInfoVo;
        });
        return dataTable;
    }
//    @ApiOperation("新增")
    @RequestMapping("insert")
    @PreAuthorize("@ss.hasPermi('system:record:query')")
    public void testInsert(){
        TestInfo testInfo = new TestInfo();
        testInfo.setName("zs"+Math.random());
        testService.save(testInfo);
    }
    @PostMapping("update")
    @PreAuthorize("@ss.hasPermi('system:record:query')")
//    @ApiOperation("更新")
    public void testUpdate(){
        TestInfo byId = testService.getById(29);
        byId.setName("wangwu");
        testService.updateById(byId);
    }

    @RequestMapping("delete")
    @PreAuthorize("@ss.hasPermi('system:record:query')")
    public void testDelete(){

    }

}
