package com.ruoyi.project.test.domain;

import lombok.Data;

@Data
public class TestInfoVo {
    private String name;
}
