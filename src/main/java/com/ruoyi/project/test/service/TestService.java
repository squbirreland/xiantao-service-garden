package com.ruoyi.project.test.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.test.domain.TestInfo;
import com.ruoyi.project.test.mapper.TestMapper;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
public class TestService extends ServiceImpl<TestMapper, TestInfo> {
}
