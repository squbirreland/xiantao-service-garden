package com.ruoyi.project.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.test.domain.TestInfo;

public interface TestMapper extends BaseMapper<TestInfo> {
}
