package com.ruoyi.project.inner.constant;


import com.ruoyi.project.inner.utils.MacAddressBase;

/**
 * @author Squbi
 */
public interface MqttTopic {
    String SERVER_TOPIC = "SERVER";
    String LOCAL_TOPIC = MacAddressBase.getLocal();
    String CALLBACK_ENDING = "_SERVER";
    String TERMINAL_ENDING = "_TERMINAL";
    String CALLBACK_TOPIC = LOCAL_TOPIC + CALLBACK_ENDING;
    String TERMINAL_TOPIC = LOCAL_TOPIC + TERMINAL_ENDING;
}
