package com.ruoyi.project.inner.config;


import com.ruoyi.common.core.lang.UUID;
import com.ruoyi.project.inner.constant.MqttTopic;
import com.ruoyi.project.inner.mqtt.MqttClientFactory;
import com.ruoyi.project.inner.mqtt.MqttTemplate;
import com.ruoyi.project.inner.mqtt.constant.MqttInitializer;
import com.ruoyi.project.inner.service.impl.MqttCallBackService;
import com.ruoyi.project.inner.service.impl.MqttInitImpl;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Squbi
 */
@Configuration
public class MqttConfig {
    @Bean
    public MqttTemplate template(MqttClientFactory factory, MqttCallBackService mqttCallBackService, MqttInitializer init) {
        MqttTemplate template = null;
        try {
            template = new MqttTemplate(factory, MqttTopic.SERVER_TOPIC + UUID.randomUUID(), mqttCallBackService, init);
        } catch (MqttException e) {
            e.printStackTrace();
        }
        return template;
    }
}
