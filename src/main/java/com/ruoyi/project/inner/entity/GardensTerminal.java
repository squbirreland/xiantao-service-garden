package com.ruoyi.project.inner.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author Squbi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GardensTerminal {
    @TableId
    private Integer terminalId;
    private String terminalMac;
    private LocalDateTime terminalLastLogin;
}
