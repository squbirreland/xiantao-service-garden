package com.ruoyi.project.inner.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Squbi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GardensUserTerminal {
    @TableId
    private Integer userTerminalId;
    private Integer userId;
    private Integer terminalId;
}
