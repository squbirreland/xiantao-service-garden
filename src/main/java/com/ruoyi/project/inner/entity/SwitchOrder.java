package com.ruoyi.project.inner.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Squbi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SwitchOrder {
    private int id;
    private int operation;
}
