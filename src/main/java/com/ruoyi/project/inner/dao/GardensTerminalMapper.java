package com.ruoyi.project.inner.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.inner.entity.GardensTerminal;

/**
 * @author Squbi
 */
public interface GardensTerminalMapper extends BaseMapper<GardensTerminal> {

}
