package com.ruoyi.project.inner.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.inner.entity.GardensUserTerminal;

/**
 * @author Squbi
 */
public interface GardensUserTerminalMapper extends BaseMapper<GardensUserTerminal> {
    String selectMacByUserId(Long userId);
}
