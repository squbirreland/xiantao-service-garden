package com.ruoyi.project.inner.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author Squbi
 */
@Component
public class HttpClient {
    @Autowired
    private RestTemplate restTemplate;

    public String request(String url, HttpMethod method, String json){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

        ResponseEntity<String> response = restTemplate.exchange(url,method,requestEntity,String.class);
        return response.getBody();
    }
}
