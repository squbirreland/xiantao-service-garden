package com.ruoyi.project.inner.utils;

import com.ruoyi.project.inner.constant.MqttTopic;

/**
 * Mac地址与Topic的规范
 *
 * @author Squbi
 * @date 2022/2/17 10:41
 */
public class MacTopicUtil {
    private MacTopicUtil() {
    }

    public static String getRedisKeyByTopic(String topic, String uuid) {
        return topic + ":" + uuid;
    }

    public static String getRedisKeyByMac(String mac, String uuid) {
        return getMacCallbackTopic(mac) + ":" + uuid;
    }

    public static String getMacCallbackTopic(String mac) {
        return mac + MqttTopic.CALLBACK_ENDING;
    }
}
