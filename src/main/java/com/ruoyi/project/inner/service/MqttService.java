package com.ruoyi.project.inner.service;


import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensMqttCallback;

/**
 * @author Squbi
 */
public interface MqttService {
    JsonResult userCall(String type, String url, Object data);

    String call(String mac, GardensMqttCallback callback);

    void subscribe(String topic, int qos);
}
