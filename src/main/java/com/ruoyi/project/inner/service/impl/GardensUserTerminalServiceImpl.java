package com.ruoyi.project.inner.service.impl;

import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.security.LoginUser;
import com.ruoyi.framework.security.service.TokenService;
import com.ruoyi.project.inner.dao.GardensUserTerminalMapper;
import com.ruoyi.project.inner.service.GardensUserTerminalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Squbi
 */
@Service
public class GardensUserTerminalServiceImpl implements GardensUserTerminalService {
    @Autowired
    private GardensUserTerminalMapper mapper;

    @Autowired
    private TokenService tokenService;

    @Override
    public String getUserTerminal() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();
        return mapper.selectMacByUserId(userId);
    }
}
