package com.ruoyi.project.inner.mqtt;

import lombok.Data;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;

/**
 * @author Squbi
 */
@Data
public class MqttMessageFactory {
    private boolean mutable;
    private boolean duplicate;
    private boolean retained;
    private int qos;

    public MqttMessageFactory(boolean mutable, boolean duplicate, boolean retained, int qos) {
        this.mutable = mutable;
        this.duplicate = duplicate;
        this.retained = retained;
        this.qos = qos;
    }

    public MqttMessageFactory() {
        this.mutable = true;
        this.duplicate = true;
        this.retained = true;
        this.qos = 2;
    }

    public MqttMessage createMessage(byte[] payload) {
        return createMessage(payload, null);
    }

    public MqttMessage createMessage(byte[] payload, MqttProperties properties) {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setMutable(mutable);
        mqttMessage.setDuplicate(duplicate);
        mqttMessage.setQos(qos);
        mqttMessage.setRetained(retained);
        mqttMessage.setPayload(payload);
        if (properties != null) {
            mqttMessage.setProperties(properties);
        }
        return mqttMessage;
    }
}
