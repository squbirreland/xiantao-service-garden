package com.ruoyi.project.inner.mqtt.constant;

/**
 * @author Squbi
 */

public interface MqttReasonCode {
    int SUCCESS = 0;
    int CONTINUE_AUTHENTICATION = 24;
    int AUTHENTICATE = 25;
}
