package com.ruoyi.project.inner.mqtt.constant;

import org.eclipse.paho.mqttv5.common.MqttMessage;

/**
 * @author Squbi
 */
public interface MqttMessageArrivedCall {
    /**
     * 当消息从Mqtt到达时需要执行的方法
     *
     * @param message 消息
     * @param topic   主题
     * @author Squbi
     * @date 2021/8/20 10:19
     */
    void exec( String topic, MqttMessage message);
}
