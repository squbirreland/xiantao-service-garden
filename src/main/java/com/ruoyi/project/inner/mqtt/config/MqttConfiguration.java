package com.ruoyi.project.inner.mqtt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Squbi
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "mqtt")
public class MqttConfiguration {
    private String serverUri;
    private Boolean autoReconnect;
    private Boolean cleanStart;
    private Integer connectionTimeout;
}
