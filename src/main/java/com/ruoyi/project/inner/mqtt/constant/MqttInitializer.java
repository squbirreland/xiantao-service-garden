package com.ruoyi.project.inner.mqtt.constant;

import org.eclipse.paho.mqttv5.client.MqttClient;
import org.eclipse.paho.mqttv5.common.MqttException;

/**
 * @author Squbi
 */
public interface MqttInitializer {
    /**
     * 初始化后可执行的方法
     *
     * @param client mqtt client
     * @throws MqttException mqtt捕获异常
     * @author Squbi
     * @date 2021/11/16 15:08
     */
    void init(MqttClient client) throws MqttException;
}
