package com.ruoyi.project.inner.mqtt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Squbi
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "mqtt.message")
public class MqttMessageConfiguration {
    private Boolean duplicate;
    private Boolean mutable;
    private Boolean retained;
    private Integer qos;
}
