package com.ruoyi.project.inner.controller;

import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensThresholdDto;
import com.ruoyi.project.inner.controller.dto.GardensThresholdQueryDto;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Squbi
 */
@RestController
public class GardensThresholdController {
    @Autowired
    private MqttService service;

    @ApiOperation("获取阈值")
    @GetMapping("/threshold")
    public JsonResult getThreshold() {
        return service.userCall("GET", "/threshold", null);
    }

    @ApiOperation("获取设备id对应的阈值设置")
    @GetMapping("/threshold/{deviceId}")
    public JsonResult getThreshold(@PathVariable("deviceId") Integer deviceId) {
        return service.userCall("GET", "/threshold/" + deviceId, null);
    }

    @PostMapping("/threshold/query")
    public JsonResult queryThreshold(@RequestBody GardensThresholdQueryDto dto) {
        return service.userCall("POST", "/threshold/query" , dto);
    }


    @ApiOperation("thresholdFiled对于不同类型的设备的字段不同\n" + "类型 1 天气 可选字段有: 湿度 weatherHumidity; 温度 weatherTemperature; 噪音 weatherNoise; pm2.5 weatherPm25; pm10 weatherPm10;\n" + "类型 2 土壤 可选字段有: 湿度 soilMoisture; 温度 soilTemperature; 导电 soilElectrical; 氮 soilNitrogen; 磷 soilPhosphorus; 钾 soilPotassium; 盐度 soilSalinity;\n")
    @PostMapping("/threshold")
    public JsonResult insertThreshold(@RequestBody GardensThresholdDto dto) {
        return service.userCall("POST", "/threshold", dto);
    }

    @ApiOperation("修改阈值设置")
    @PutMapping("/threshold")
    public JsonResult update(@RequestBody GardensThresholdDto dto) {
        return service.userCall("PUT", "/threshold", dto);
    }

    @ApiOperation("删除阈值设置")
    @DeleteMapping("/threshold")
    public JsonResult deleteThreshold(Integer deviceId) {
        return service.userCall("DELETE", "/threshold?deviceId=" + deviceId, null);
    }
}