package com.ruoyi.project.inner.controller;

import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensDeviceAreaDto;
import com.ruoyi.project.inner.controller.dto.GardensDeviceDto;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Squbi
 */
@Api("终端调用 - 设备")
@RestController
public class GardensDeviceController {

    @Autowired
    private MqttService service;

    @ApiOperation("设备集")
    @PostMapping("/device")
    public JsonResult selectByDto(@RequestBody GardensDeviceDto dto) {
        return service.userCall("POST", "/device", dto);
    }

    @ApiOperation("设备与区域映射")
    @PostMapping("/device/area")
    public JsonResult selectAllJoinArea(@RequestBody GardensDeviceAreaDto dto) {
        return service.userCall("POST", "/device/area", dto);
    }

    @ApiOperation("设备区域映射与可能的状态")
    @PostMapping("/device/area/status")
    public JsonResult selectJoinAreaStatus(@RequestBody GardensDeviceAreaDto dto) {
        return service.userCall("POST", "/device/area/status", dto);
    }

    @ApiOperation("继电器设备概览")
    @GetMapping("/device/switch")
    public JsonResult selectAllJoinSwitch(Integer deviceId) {
        return service.userCall("GET", "/device/switch?deviceId=" + deviceId, null);
    }

    @ApiOperation("并联开关设备概览")
    @GetMapping("/device/paralleling")
    public JsonResult selectAllJoinParalleling(Integer deviceId) {
        return service.userCall("GET", "/device/paralleling?deviceId=" + deviceId, null);
    }
}