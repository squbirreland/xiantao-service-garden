package com.ruoyi.project.inner.controller.dto;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Squbi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GardensDeviceAreaDto {
    private Integer deviceType;
    private Integer areaId;
    private Integer deviceId;
}
