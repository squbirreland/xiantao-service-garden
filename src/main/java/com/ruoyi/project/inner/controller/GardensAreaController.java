package com.ruoyi.project.inner.controller;

import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensArea;
import com.ruoyi.project.inner.controller.dto.GardensAreaDto;
import com.ruoyi.project.inner.controller.dto.GardensAreaMappingDto;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author Squbi
 */
@Api("终端调用 - 区域")
@RestController
public class GardensAreaController {
    @Autowired
    private MqttService service;

    @ApiOperation("创建区域")
    @PostMapping("/area")
    public JsonResult newArea(@RequestBody GardensAreaDto dto) {
        return service.userCall("POST", "/area", dto);
    }

    @ApiOperation("删除区域")
    @DeleteMapping("/area")
    public JsonResult deleteArea(int areaId) {
        return service.userCall("DELETE", "/area?areaId=" + areaId, null);
    }

    @ApiOperation("查询区域")
    @GetMapping("/area")
    public JsonResult selectArea() {
        return service.userCall("GET", "/area", null);
    }

    @ApiOperation("更新区域")
    @PutMapping("/area")
    public JsonResult updateArea(@RequestBody GardensArea area) {
        return service.userCall("PUT", "/area", area);
    }

    @ApiOperation("新的区域映射")
    @PostMapping("/area/mapping")
    public JsonResult newMapping(@RequestBody GardensAreaMappingDto dto) {
        return service.userCall("POST", "/area/mapping", dto);
    }

    @ApiOperation("删除区域映射")
    @DeleteMapping("/area/mapping")
    public JsonResult deleteMapping(@RequestBody GardensAreaMappingDto dto) {
        return service.userCall("DELETE", "/area/mapping", dto);
    }

    @ApiOperation("获取区域映射")
    @GetMapping("/area/mapping")
    public JsonResult selectMapping(int areaId) {
        return service.userCall("GET", "/area/mapping?areaId=" + areaId, null);
    }
}
