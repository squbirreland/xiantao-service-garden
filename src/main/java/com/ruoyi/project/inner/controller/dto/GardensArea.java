package com.ruoyi.project.inner.controller.dto;

import lombok.Data;

/**
 * @author Squbi
 */
@Data
public class GardensArea {
    private Integer areaId;
    private String areaName;
    private String areaDescription;
}
