package com.ruoyi.project.inner.controller;

import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensDeviceSelectDto;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Squbi
 */
@Api("终端调用 - 泥土")
@RestController
public class GardensSoilController {

    @Autowired
    private MqttService service;

    @ApiOperation("土壤最新状态")
    @GetMapping("/soil/newly")
    public JsonResult getSoilNewly() {
        return service.userCall("GET", "/soil/newly", null);
    }

    @ApiOperation("土壤历史状态")
    @PostMapping("/soil/list")
    public JsonResult getSoilList(@RequestBody GardensDeviceSelectDto dto) {
        return service.userCall("POST", "/soil/list", dto);
    }
}
