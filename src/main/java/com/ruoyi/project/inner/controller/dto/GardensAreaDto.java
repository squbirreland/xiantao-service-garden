package com.ruoyi.project.inner.controller.dto;

import lombok.Data;

@Data
public class GardensAreaDto {
  private   String areaName;
  private String areaDescription;
}
