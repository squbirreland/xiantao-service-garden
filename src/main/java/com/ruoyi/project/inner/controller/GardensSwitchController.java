package com.ruoyi.project.inner.controller;

import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Squbi
 */
@Api("终端调用 - 继电器")
@RestController
public class GardensSwitchController {
    @Autowired
    private MqttService service;

    @ApiOperation("继电器最新状态")
    @GetMapping("/switch/newly")
    public JsonResult selectNewly() {
        return service.userCall("GET", "/switch/newly", null);
    }
}
