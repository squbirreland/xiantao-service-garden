package com.ruoyi.project.inner.controller;

import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensDeviceSelectDto;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Squbi
 */
@Api("终端调用 - 雨雪")
@RestController
public class GardensSleetController {
    @Autowired
    private MqttService service;

    @ApiOperation("雨雪最新状态")
    @GetMapping("/sleet/newly")
    public JsonResult getSleetNewly() {
        return service.userCall("GET", "/sleet/newly", null);
    }

    @ApiOperation("雨雪历史状态")
    @PostMapping("/sleet/list")
    public JsonResult getSleetList(@RequestBody GardensDeviceSelectDto dto) {
        return service.userCall("POST", "/sleet/list", dto);
    }
}
