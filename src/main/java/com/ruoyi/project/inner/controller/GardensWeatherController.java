package com.ruoyi.project.inner.controller;


import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensDeviceSelectDto;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Squbi
 */
@Api("终端调用 - 天气")
@RestController
public class GardensWeatherController {
    @Autowired
    private MqttService service;

    @GetMapping("/weather/newly")
    @ApiOperation("获取天气最新数据")
    public JsonResult getWeatherNewly() {
        return service.userCall("GET", "/weather/newly", "{}");
    }

    @PostMapping("/weather/list")
    @ApiOperation("获取天气历史数据")
    public JsonResult getSoilList(@RequestBody GardensDeviceSelectDto dto) {
        return service.userCall("POST", "/weather/list", dto);
    }

    @ApiOperation("获取当日温差")
    @GetMapping("/weather/difference")
    public JsonResult getDifference() {
        return service.userCall("GET", "/weather/difference", null);
    }
}
