package com.ruoyi.project.inner.controller.dto;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Squbi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GardensMqttCallback {
    private String uuid;
    private String type;
    private String url;
    private String data;

    public GardensMqttCallback(String type, String url, Object data) {
        this.type = type;
        this.url = url;
        this.uuid = "";
        this.data = JSON.toJSONString(data);
    }
}
