package com.ruoyi.project.inner.controller.dto;


import lombok.Data;


/**
 * @author Squbi
 */

@Data
public class GardensDeviceDto {
    private Integer deviceId;
    private Integer deviceType;
    private Integer pageIndex;
    private Integer pageSize;

    public GardensDeviceDto(Integer pageIndex, Integer pageSize, Integer deviceId, Integer deviceType) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.deviceId = deviceId;
        this.deviceType = deviceType;
    }
}
