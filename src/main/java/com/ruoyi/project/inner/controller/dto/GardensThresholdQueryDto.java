package com.ruoyi.project.inner.controller.dto;

import lombok.Data;

/**
 * @author Squbi
 */
@Data
public class GardensThresholdQueryDto {
    private Integer deviceId;
    private Integer callDeviceId;
    private Integer pageIndex;
    private Integer pageSize;
}

