package com.ruoyi.project.inner.controller.dto;

import lombok.Data;

@Data
public class GardensAreaMappingDto {
    private Integer areaId;
    private Integer deviceId;
}
