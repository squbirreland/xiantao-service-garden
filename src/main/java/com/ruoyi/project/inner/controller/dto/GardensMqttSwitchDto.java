package com.ruoyi.project.inner.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Squbi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GardensMqttSwitchDto {
    private Integer deviceId;
    private Integer operation;
}
