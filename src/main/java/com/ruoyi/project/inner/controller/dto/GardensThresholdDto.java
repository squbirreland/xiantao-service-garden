package com.ruoyi.project.inner.controller.dto;

import lombok.Data;

/**
 * @author Squbi
 */
@Data
public class GardensThresholdDto {
    private Integer deviceId;
    private String thresholdFiled;
    private Double thresholdMax;
    private Double thresholdMin;
    private Integer callDeviceId;
    private Integer callOperation;
}
