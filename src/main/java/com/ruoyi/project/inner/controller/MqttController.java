package com.ruoyi.project.inner.controller;


import com.ruoyi.project.common.JsonResult;
import com.ruoyi.project.inner.controller.dto.GardensMqttSwitchDto;
import com.ruoyi.project.inner.service.MqttService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Squbi
 */
@Api("终端调用 - 控制")
@RestController
public class MqttController {
    @Autowired
    private MqttService service;

    @ApiOperation("开关控制")
    @PostMapping("/mqtt/switch")
    public JsonResult localSwitch(@RequestBody GardensMqttSwitchDto dto) {
        return service.userCall("POST", "/mqtt/switch", dto);
    }
}
