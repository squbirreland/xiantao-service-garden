package com.ruoyi.framework.config.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * <p></p>
 *
 * @author axzsd
 * @version 1.0
 * @date 2019 2019/8/16 9:58
 */
public class JacksonJsonLongSerializer extends JsonSerializer<Long> {
    @Override
    public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (value != null)
            gen.writeString(Long.toString(value));
    }
}
