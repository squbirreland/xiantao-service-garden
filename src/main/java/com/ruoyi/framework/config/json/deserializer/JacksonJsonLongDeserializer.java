package com.ruoyi.framework.config.json.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * <p></p>
 *
 * @author axzsd
 * @version 1.0
 * @date 2019 2019/8/16 9:58
 */
public class JacksonJsonLongDeserializer extends JsonDeserializer<Long> {
    @Override
    public Long deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (p.getText() != null)
            return Long.valueOf(p.getText().trim());
        return 0L;
    }
}
