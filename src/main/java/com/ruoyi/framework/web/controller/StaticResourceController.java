package com.ruoyi.framework.web.controller;

import com.ruoyi.common.utils.file.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 */
@Controller
@RequestMapping("/static")
public class StaticResourceController {
    @RequestMapping("img")
    public void show(@RequestParam("f") String filePath, HttpServletResponse servletResponse) throws IOException {
        servletResponse.setContentType("image/jpeg");
        FileUtils.writeBytes(filePath,servletResponse.getOutputStream());
    }
}
