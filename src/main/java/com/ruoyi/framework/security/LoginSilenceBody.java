package com.ruoyi.framework.security;

import lombok.Data;

/**
 * @author Squbi
 */
@Data
public class LoginSilenceBody {
    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;
}
